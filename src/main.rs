use chrono::prelude as chrono;
use handlebars::{Handlebars, RenderContext, Helper, Context, HelperResult, Output};
use rayon::prelude::*;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::cmp::{Eq, PartialEq};
use std::collections::HashMap;
use std::hash::Hash;
use std::io;
use std::io::{BufRead, BufReader, Write};
use std::fs;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::string::String;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct Opt {
    #[structopt(short="f", long="filter", parse(from_str))]
    filter: Option<String>,

    #[structopt(short="C", parse(from_os_str))]
    cwd: Option<PathBuf>,

    /// Input path
    #[structopt(parse(from_os_str))]
    input: PathBuf,

    /// Output path
    #[structopt(parse(from_os_str))]
    output: PathBuf,
}

#[derive(Clone)]
#[derive(Eq)]
#[derive(Hash)]
#[derive(PartialEq)]
#[derive(Serialize, Deserialize)]
struct Author {
    name: String,
    email: String,
}

impl Author {
    fn new(name: &str, email: &str) -> Author {
        Author { name: name.to_string(), email: email.to_string() }
    }
}

struct MailMap {
    map: HashMap<String,Author>,
}

impl MailMap {
    fn new() -> MailMap {
        MailMap { map: HashMap::new() }
    }

    fn parse(filename: &Path) -> io::Result<MailMap> {
        let mut map = HashMap::new();
        let re = Regex::new(r"(^[^<]*)<([^>]*)>[^<]*<([^>]*)>").unwrap();

        let file = fs::File::open(filename)?;
        for line in BufReader::new(file).lines() {
            let line = line?;
            if let Some(cap) = re.captures(&line) {
                map.insert(cap.get(3).unwrap().as_str().to_string(),
                           Author::new(cap.get(1).unwrap().as_str(),
                                       cap.get(2).unwrap().as_str()));
            }
        }
        Ok(MailMap{map: map})
    }

    fn map<'a>(&'a self, src: &'a Author) -> &'a Author {
        match self.map.get(&src.email) {
            Some(x) => x,
            None => src,
        }
    }
}

#[derive(Clone)]
#[derive(Serialize, Deserialize)]
struct AuthorLines {
    author: Author,
    lines: u32,
}

impl AuthorLines {
    fn new(author: Author, lines: u32) -> AuthorLines {
        AuthorLines { author: author, lines: lines }
    }

    fn empty() -> AuthorLines {
        AuthorLines::new(Author::new("", ""), 0)
    }
}

struct Stats {
    author_lines: HashMap<Author,u32>,
    total_lines: u32,
}

impl Stats {
    fn new() -> Stats {
        Stats {
            author_lines: HashMap::new(),
            total_lines: 0
        }
    }

    fn add_lines(&mut self, author: &Author, lines: u32) {
        if self.author_lines.contains_key(author) {
            let count = self.author_lines.get_mut(author).unwrap();
            *count += lines;
        } else {
            self.author_lines.insert(author.clone(), lines);
        }
        self.total_lines += lines;
    }

    fn merge(&mut self, other: &Stats) {
        for (author, lines) in &other.author_lines {
            self.add_lines(author, *lines);
        }
    }

    fn as_vec(&self) -> Vec<AuthorLines> {
        self.author_lines.iter().map(|x| AuthorLines {
            author: x.0.clone(),
            lines: *x.1,
        }).collect()
    }
}

struct PathStats {
    path: PathBuf,
    children: Vec<PathStats>,
    stats: Stats,
}

impl PathStats {
    fn new(path: &Path) -> PathStats {
        PathStats {
            path: PathBuf::from(path),
            children: Vec::new(),
            stats: Stats::new(),
        }
    }

    fn empty() -> PathStats {
        PathStats::new(&Path::new(""))
    }

    fn add_lines(&mut self, author: &Author, lines: u32) {
        assert!(self.children.is_empty());
        self.stats.add_lines(author, lines);
    }

    fn add_child(&mut self, child: PathStats) {
        self.stats.merge(&child.stats);
        self.children.push(child);
    }

    fn is_empty(&self) -> bool {
        self.stats.total_lines == 0
    }
}

fn find_file_in_parent_recur(mut path: PathBuf, filename: &str) -> Option<PathBuf> {
    path.push(filename);
    if path.as_path().exists() {
        Some(path)
    } else {
        path.pop();
        if path.pop() {
            find_file_in_parent_recur(path, filename)
        } else {
            None
        }
    }
}

fn find_file_in_parent(path: &Path, filename: &str) -> Option<PathBuf> {
    find_file_in_parent_recur(PathBuf::from(path), filename)
}

struct Parser {
    input: PathBuf,
    filter: Option<Regex>,
    mailmap: MailMap,
    parallel: bool,
}

impl Parser {
    fn open(input: &Path, filter: &Option<Regex>) -> io::Result<Parser> {
        let pwd = fs::canonicalize(".")?;
        let mailmap = match find_file_in_parent(&pwd, ".mailmap") {
            Some(mm_path) => MailMap::parse(&mm_path)?,
            None => MailMap::new(),
        };

        Ok(Parser {
            input: PathBuf::from(input),
            filter: filter.clone(),
            mailmap: mailmap,
            parallel: true,
        })
    }

    fn filter_file_path(&self, path: &Path) -> bool {
        match &self.filter {
            Some(filter) => {
                match path.file_name().unwrap().to_str() {
                    Some(s) => {
                        filter.is_match(s)
                    },
                    None => false,
                }
            },
            None => true,
        }
    }

    fn parse_file(&self, infile: &Path) -> io::Result<PathStats> {
        println!("Blaming {:?}", infile);

        let cmd = Command::new("git")
                          .arg("blame")
                          .arg("--follow") // Ignore file moves
                          .arg("-w") // Ignore whitespace changes
                          .arg("-M") // See through line moves
                          .arg("-C") // See through line copies
                          .arg("-p") // Machine readable
                          .arg(infile)
                          .stdin(Stdio::null())
                          .stderr(Stdio::null())
                          .stdout(Stdio::piped())
                          .spawn()?;

        let commit_re = Regex::new(r"^[0-9a-f]{40}").unwrap();
        let name_re = Regex::new(r"^author (.*)$").unwrap();
        let email_re = Regex::new(r"^author-mail <(.*)>$").unwrap();
        let line_re = Regex::new(r"^\t").unwrap();

        let mut commits = HashMap::new();
        let mut current = None;

        let reader = BufReader::new(cmd.stdout.unwrap());
        for line in reader.lines() {
            let line = line?;
            if let Some(mat) = commit_re.find(&line) {
                let sha = mat.as_str();
                if !commits.contains_key(sha) {
                    commits.insert(String::from(sha), AuthorLines::empty());
                }
                current = commits.get_mut(sha);
                continue;
            }

            // At this point, we have a commit
            let current = current.as_mut().unwrap();

            if let Some(cap) = name_re.captures(&line) {
                assert!(current.author.name.is_empty());
                current.author.name = String::from(cap.get(1).unwrap().as_str());
            } else if let Some(cap) = email_re.captures(&line) {
                assert!(current.author.email.is_empty());
                current.author.email = String::from(cap.get(1).unwrap().as_str());
            } else if line_re.is_match(&line) {
                current.lines += 1;
            }
        }

        let mut stats = PathStats::new(infile);
        for (_, commit) in commits.drain() {
            stats.add_lines(self.mailmap.map(&commit.author), commit.lines);
        }

        Ok(stats)
    }

    fn parse_dir(&self, indir: &Path) -> io::Result<PathStats> {
        println!("Blaming {:?}", indir);

        /* Collect into a vec so we can parallel iterate */
        let mut entries = Vec::new();
        for entry in fs::read_dir(indir)? {
            entries.push(entry?.path());
        }
        entries.sort();

        let children: Vec<io::Result<PathStats>> = if self.parallel {
            entries.par_iter().map(|e| self.parse_path(e)).collect()
        } else {
            entries.iter().map(|e| self.parse_path(e)).collect()
        };

        let mut stats = PathStats::new(indir);
        for child in children {
            let child = child?;
            if !child.is_empty() {
                stats.add_child(child);
            }
        }

        Ok(stats)
    }

    fn parse_path(&self, inpath: &Path) -> io::Result<PathStats> {
        let metadata = fs::metadata(inpath)?;
        if metadata.is_file() && self.filter_file_path(inpath) {
            self.parse_file(inpath)
        } else if metadata.is_dir() {
            self.parse_dir(inpath)
        } else {
            Ok(PathStats::empty())
        }
    }

    fn parse_all(&self) -> io::Result<PathStats> {
        self.parse_path(&self.input)
    }
}

const HTML_HEADER: &'static str = r#"
<meta charset="utf-8"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic' type='text/css' rel='stylesheet' />
<style>
  body {
    font-family: "Open Sans", sans-serif;
    font-size: 14px;
    color: black;
  }

  a {
    color: black;
  }

  th {
    background-color: #3366cc;
    color: white;
  }

  th, td {
    padding: 5px;
    text-align: left;
    border-collapse: collapse;
  }

  tr:nth-child(even) {
    background-color: #f2f2f2;
  }
</style>
"#;

const WRITER_SUMMARY_HTML: &'static str = r#"
<!DOCTYPE html>
<html lang="en-US">
<head>
  {{> header}}
  <title>git-blame: {{path}}</title>
</head>
<body>

<h1>git-blame: {{#each path_vec}}{{#if this.url}}<a href="{{this.url}}">{{/if}}{{this.name}}{{#if this.url}}</a>{{/if}}/{{/each}}{{file}}</h1>

<h2>Top Contributors:</h2>

<div id="piechart"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Author', 'Lines of code'],
{{#each top_ten}}
  ['{{this.author.name}}', {{this.lines}}],
{{/each}}
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>

<h2>All Contributors:</h2>

<table>
  <tr>
    <th>Name</th>
    <th>E-mail</th>
    <th>Lines</th>
  </tr>
{{#each authors}}
  <tr>
    <td>{{this.author.name}}</td>
    <td>{{this.author.email}}</td>
    <td>{{this.lines}} ({{(percent this.lines ../total_lines)}})</td>
  </tr>
{{/each}}
</table>

{{#if children}}
<h2>Contents:</h2>
<ul>
{{#each children}}
  <li><a href="{{this.html}}">{{this.infile}}</a></li>
{{/each}}
</ul>
{{/if}}

<p>Generated on {{date}}</p>

</body>
</html>
"#;

struct Writer<'a> {
    path: PathBuf,
    hb: Handlebars<'a>,
}

fn percent_helper(h: &Helper, _: &Handlebars, _: &Context,
                  _: &mut RenderContext, out: &mut dyn Output) -> HelperResult {
    let frac = h.param(0).unwrap().value().as_f64().unwrap();
    let total = h.param(1).unwrap().value().as_f64().unwrap();

    out.write(&format!("{:.1} %", frac * 100f64 / total))?;
    Ok(())
}

impl Writer<'_> {
    fn open(path: &Path) -> io::Result<Writer<'static>> {
        let mut hb = Handlebars::new();
        hb.register_helper("percent", Box::new(percent_helper));
        if let Err(e) = hb.register_partial("header", HTML_HEADER) {
            panic!("Failed to parse template: {}", e);
        }
        if let Err(e) = hb.register_template_string("summary_html",
                                                    WRITER_SUMMARY_HTML) {
            panic!("Failed to parse template: {}", e);
        }

        Ok(Writer {
            path: PathBuf::from(path),
            hb: hb,
        })
    }

    fn get_html_path(&self, base: &Path, stats: &PathStats) -> PathBuf {
        let mut path = self.path.clone();
        path.push(&stats.path.strip_prefix(base).unwrap());
        if stats.children.is_empty() {
            let mut str_path = path.into_os_string();
            str_path.push(".html");
            path = PathBuf::from(str_path)
        } else {
            path.push("index.html");
        }
        path
    }

    fn write_summary_html(&self, outfile: &Path, base: &Path, stats: &PathStats) -> io::Result<()> {
        let mut authors = stats.stats.as_vec();
        authors.sort_by(|a, b| b.lines.cmp(&a.lines));

        // Compute a "path" with urls up the chain
        let mut path_vec = Vec::new();
        let mut up = PathBuf::new();
        if !stats.children.is_empty() {
            up.push("..");
        }
        if let Some(parent) = stats.path.parent() {
            for anc in parent.ancestors() {
                let name = match anc.file_name() {
                    Some(name) => name,
                    None => break,
                };

                up.push("index.html");
                let url = if anc.starts_with(base) {
                    up.clone()
                } else {
                    PathBuf::from("")
                };
                up.pop();
                up.push("..");

                path_vec.push(json!({
                    "name": Path::new(name),
                    "url": &url.as_path(),
                }));
            }
            path_vec.reverse();
        }

        let top_ten = if authors.len() <= 10 {
            authors.clone()
        } else {
            let mut v = Vec::new();
            let mut other_lines = stats.stats.total_lines;
            for al in &authors[0..9] {
                other_lines -= al.lines;
                v.push(al.clone());
            }
            v.push(AuthorLines::new(Author::new("Others", ""), other_lines));
            v
        };

        let mut children = Vec::new();
        for child in &stats.children {
            children.push(json!({
                "infile": &child.path,
                "html": self.get_html_path(base, &child)
                            .strip_prefix(outfile.parent().unwrap()).unwrap(),
            }));
        }

        let data = json!({
            "file": Path::new(stats.path.file_name().unwrap()),
            "path": stats.path,
            "date": chrono::Utc::now().to_string(),
            "path_vec": path_vec,
            "children": children,
            "authors": &authors,
            "top_ten": &top_ten,
            "total_lines": stats.stats.total_lines,
        });

        let s = match self.hb.render("summary_html", &data) {
            Ok(s) => s,
            Err(e) => panic!("Failed to render: {}", e),
        };

        fs::File::create(outfile)?.write(&s.into_bytes())?;
        Ok(())
    }

    fn write_summaries_recur(&self, base: &Path, stats: &PathStats) -> io::Result<()> {
        let htmlfile = self.get_html_path(base, stats);
        println!("Writing {:?}", htmlfile);

        fs::create_dir_all(&htmlfile.parent().unwrap())?;
        self.write_summary_html(&htmlfile, base, stats)?;

        let res: Vec<io::Result<()>> = stats.children.par_iter().map(|child| {
            self.write_summaries_recur(base, child)
        }).collect();

        for child_res in res {
            child_res?;
        }
        Ok(())
    }

    fn write_summaries(&self, stats: &PathStats) -> io::Result<()> {
        self.write_summaries_recur(&stats.path, stats)
    }
}

fn main() -> io::Result<()> {
    let opt = Opt::from_args();

    if let Some(cwd) = opt.cwd {
        std::env::set_current_dir(&cwd)?;
    }

    let filter = match opt.filter {
        Some(re) => Some(Regex::new(&re).expect("Invalid filter expression")),
        None => None,
    };

    let stats = Parser::open(&opt.input, &filter)?.parse_all()?;
    Writer::open(&opt.output)?.write_summaries(&stats)?;

    Ok(())
}
